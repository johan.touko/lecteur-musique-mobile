import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class MusiqueService {

  constructor(private httpClient : HttpClient) { }

  getalbums(){
    return this.httpClient.get("assets/albums.json");
  }
  getsong(){
    return this.httpClient.get("assets/song.json");
  }
}
