import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MusiqueService } from 'src/app/services/musique.service';


@Component({
  selector: 'app-albums',
  templateUrl: './albums.page.html',
  styleUrls: ['./albums.page.scss'],
})
export class AlbumsPage implements OnInit {
  search : string = "";
  public musique: any;

  constructor(private albums : MusiqueService , private router : Router) { }

  ngOnInit() {
    this.albums.getalbums().subscribe((rep) =>{
      this.musique = rep;
    })
  }

  // showdetails(obj:any) : void{localStorage.setItem('key' , JSON.stringify(obj))}

}
