import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailalbumsPage } from './detailalbums.page';

const routes: Routes = [
  {
    path: '',
    component: DetailalbumsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailalbumsPageRoutingModule {}
