import { Component, OnInit } from '@angular/core';
import { MusiqueService } from 'src/app/services/musique.service';
import { ActivatedRoute, Router } from '@angular/router';
import {NativeAudio} from '@capgo/native-audio'
import { accueilvariable } from '../accueil/accueil.page';
export let variable = {
  song:{}
}
@Component({
  selector: 'app-detailalbums',
  templateUrl: './detailalbums.page.html',
  styleUrls: ['./detailalbums.page.scss'],
})
export class DetailalbumsPage implements OnInit {
  public Msong: any;
  testposition : number = 2;
  id:any;
  img:any;
  aut:any;
  titre:any;
  couleur:any;
  temprestant!: string;
  curr_paying_file:any;
  position:any=0;
  last_position:any;
  duration:any=-1;

  constructor(private detailalbums : MusiqueService , private router : ActivatedRoute) {
    this.id = this.router.snapshot.paramMap.get('id');
    this.img = this.router.snapshot.paramMap.get('image');
    this.aut = this.router.snapshot.paramMap.get('artiste')
    this.titre = this.router.snapshot.paramMap.get('titre')
    this.couleur = this.router.snapshot.paramMap.get('color')
   }

  ngOnInit()  {
    this.detailalbums.getsong().subscribe((rep) =>{
      this.Msong = rep;
      this.Msong = this.Msong.filter((x:{albumid: any;})=>x.albumid==this.id);

      this.Msong.forEach(async(el:any) =>{
        await this.charger(el);
      })
    })
    this.testposition = 1 ;
  }

  async charger(sg:any){
    await NativeAudio.preload({ 
      assetId: sg.id.toString(),
      assetPath: sg.url,
      audioChannelNum: 1, 
      isUrl: false
      });
  }
  async play(sg:any){  
    console.log(variable.song,"play details 1")
    this.fin(variable.song)
    variable.song = sg
    accueilvariable.song = sg;
    console.log(variable.song,"play variable ")
    console.log(accueilvariable.song,"play accueilvariable")
    await NativeAudio.play({ 
      assetId: sg.id.toString(), 
      time: 0.0 
      });  
  }
  async fin(Object:any){
    await NativeAudio.stop({
      assetId: Object.id.toString()
    })
  }
  convertseconde(secondes: number){
    const min = Math.floor((secondes/60/60)*60);
    const sec = Math.floor(((secondes/60/60)*60 - min)*60);
    this.temprestant = min + 'm' + sec + 's'; 
    console.log("yo1" , this.temprestant);
  }

  async getDuration(objet:any){
    return await NativeAudio.getDuration({
      assetId: objet.id.toString()
    })
  .then(result => {
    this.convertseconde(result.duration);
  })
  }
  
}


