import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailalbumsPageRoutingModule } from './detailalbums-routing.module';

import { DetailalbumsPage } from './detailalbums.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailalbumsPageRoutingModule
  ],
  declarations: [DetailalbumsPage]
})
export class DetailalbumsPageModule {}
