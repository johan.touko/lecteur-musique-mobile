import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Media, MediaObject } from '@awesome-cordova-plugins/media/ngx';
import { ActivatedRoute } from '@angular/router';
import { MusiqueService } from 'src/app/services/musique.service';
import {NativeAudio} from '@capgo/native-audio'
import { throws } from 'assert';
import { variable } from '../detailalbums/detailalbums.page';
import { accueilvariable } from '../accueil/accueil.page';
import { EncodeIntoResult } from 'util';



@Component({
  selector: 'app-a-propos',
  templateUrl: './a-propos.page.html',
  styleUrls: ['./a-propos.page.scss'],
})
export class AProposPage implements OnInit {
  file!: MediaObject ;
  positionrecue!:any;
  playIcon = 'pause';
  repIcon = 'repeat-outline';
  soundIcon ='volume-mute-outline';
  dure!: number;
  curseur!: number;
  temprestant_non_convertis!: any;
  temprestant!: string;
  public sik: any;
  id_album: any;
  images:any;
  ide:any;
  min: any;
  sec: any;
  minInt: any = 0;
  secInt: any = 0;
  urle:any;
  vol=0.4;
  stop = 1;
  isPlay=0;
  song:any;
  song4:any;
  songs:any;
  ok:any;
  sik3:any;
  curr_paying_file:any;
  position:any=0;
  last_position:any;
  duration:any=-1;
  compteuraugmenter: number = 1;
  compteurdimunuer : number = 0;
  public listabum: any;
  sik1={
    id:18,
    titre:"mamou",
    albumid:1,
    auteur:"dadju",
    time:{
      sc:0,
      min:0
    },
    // path:
};
  constructor(private propos : MusiqueService , private router : ActivatedRoute, public media: Media, public plateforme: Platform) { 
    this.ide = this.router.snapshot.paramMap.get('id');
    this.urle = this.router.snapshot.paramMap.get('url');
    this.id_album = this.router.snapshot.paramMap.get('albumid');
    this.images=this.router.snapshot.paramMap.get('img')
    this.positionrecue=this.router.snapshot.paramMap.get('testposition')
  }
  ngOnInit( ) {
 
    this.propos.getsong().subscribe((rep) =>{
      this.sik3 = rep;
      this.sik = this.sik3.filter((x:{id: any;})=>x.id==this.ide);
      this.sik1 = this.sik[0];
      this.song = this.sik3.filter((x: {albumid: any;}) =>x.albumid==this.id_album)
      this.song4 = this.sik3

      this.propos.getalbums().subscribe((rep) =>{
        this.listabum = rep;
        
        this.song4.map((e: any) => {
          this.listabum.map((x: any) => {
              if (e.albumid == x.id) {
                e.pathA = x.image
              }
          })
        })
        
      })

      this.isPlay=1;
    })
      if(this.positionrecue == 1){
        variable.song = variable.song
      }else{
        variable.song = accueilvariable.song
      }
      setInterval(() => {
        this.encour(variable.song)
      })
      this.getDuration(variable.song)
  
  }
  convertseconde(secondes: number){
    const min = Math.floor((secondes/60/60)*60);
    const sec = Math.floor(((secondes/60/60)*60 - min)*60);
    this.temprestant = min + 'm' + sec + 's'; 
    console.log("yo" , this.temprestant);
  }
  convertcurentime(secondes:number){
    const min = Math.floor((secondes/60/60)*60);
    const sec = Math.floor(((secondes/60/60)*60 - min)*60);
    this.duration = min + 'm' + sec + 's'; 
    console.log("yo2" , this.duration);
  }
  changeposition(){
    // console.log('position ' + this.curseur);
    this.file.seekTo(this.curseur+1000);
    this.convertseconde(this.dure - this.curseur)
  }
  playPause() {
    if(this.playIcon == 'pause'){
      this.playIcon = 'play'
      this.pausesong();
    }else{
      this.playIcon = 'pause'
      this.playsong();
    }
  }
  volumeonof(){
    if(this.soundIcon == 'volume-mute-outline'){
      this.soundIcon = 'volume-mute-sharp'
      this.volumemuet();
    }else{
      this.soundIcon = 'volume-mute-outline'
      this.volumeconst();
    }
  }
  async charger(sg:any){
    await NativeAudio.preload({ 
      assetId: sg.ide.toString(),
      assetPath: sg.url,
      audioChannelNum: 1, 
      isUrl: false
      });
  }
  async play(sg:any){
    this.fin(variable.song)
    variable.song = sg
    accueilvariable.song = sg
    // console.log("play propos accueilvariable",accueilvariable.song);
    // console.log("play propos variable",variable.song);


    await NativeAudio.play({ 
      assetId: sg.id.toString(), 
      time: 0.0 
      });     
  }
  async repeat(){
    await NativeAudio.loop({ 
      assetId: this.sik[0].id.toString()})
  }
  async volumebas(){
    this.vol = this.vol-0.2;
    await NativeAudio.setVolume({
      assetId: this.sik[0].id.toString(),
      volume: this.vol
    })
  }
  async volumehaut(){
    this.stop=1;
    this.vol = this.vol+0.2;
      await NativeAudio.setVolume({
      assetId: this.sik[0].id.toString(),
      volume: this.vol
    })
  }
  async volumemuet(){
    this.stop=0;
    await NativeAudio.setVolume({
      assetId: this.sik[0].id.toString(),
      volume: 0.0
    })
  }
  async volumeconst(){
    this.stop=0;
    await NativeAudio.setVolume({
      assetId: this.sik[0].id.toString(),
      volume: 1
    })
  }
  async playsong(){
    this.isPlay=1
    await NativeAudio.resume({
      assetId: this.sik[0].id.toString(),
    })
  }
  async fin(Object:any){
    await NativeAudio.stop({
      assetId: Object.id.toString()
    })
  }
  async suivant(){
    if(this.positionrecue == 1){
      this.isPlay=1
      let v=0
      for (let index = 0; index < this.song.length; index++){
        if(this.song[index].id == this.ide){
          v=index
        }
      }
      this.fin(this.song[v])
      if (v==this.song.length -1 ){
        this.sik[0] = this.song[0]
        this.sik1=this.sik[0];
        this.ide= this.song[0].id
        this.play(this.song[0])
      }else{
        this.sik[0]=this.song[v+1]
        this.sik1=this.sik[0];
        this.ide= this.song[v+1].id
        this.play(this.song[v+1])
        this.getDuration(this.song[v+1])
        variable.song = this.song[v+1]
      }
    }else{

      this.isPlay=1
      let v=0
      for (let index = 0; index < this.song4.length; index++){
        if(this.song4[index].id == this.ide){
          v=index
        }
      }
      this.fin(this.song4[v])
      if (v==this.song4.length -1 ){
        this.sik[0] = this.song4[0]
        this.sik1=this.sik[0];
        this.ide= this.song4[0].id
        this.play(this.song4[0])
      }else{
        this.sik[0]=this.song4[v+1]
        this.sik1=this.sik[0];
        this.ide= this.song4[v+1].id
        this.images = this.song4[v+1].pathA
        this.play(this.song4[v+1])
        this.getDuration(this.song4[v+1])
        variable.song = this.song4[v+1]
      }
    }

  }
  async precedent(){
    if(this.positionrecue == 1){
       this.isPlay=1
      let v=0;
      for (let index = 0; index < this.song.length; index++){
        if(this.song[index].id == this.ide){ 
          v=index
        }
      }
      this.fin(this.song[v])
      if (v==0 ){
        this.sik[0] = this.song[this.song.length-1]
        this.sik1=this.sik[0];
        this.ide= this.song[this.song.length-1].id
        this.play(this.song[this.song.length-1])
      }else{
        this.sik[0]=this.song[v-1]
        this.sik1=this.sik[0];
        this.ide= this.song[v-1].id
        this.play(this.song[v-1])
        this.getDuration(this.song[v-1])
        this.getDuration(variable.song)
        variable.song = this.song[v-1]
      }
    }else{
        
      this.isPlay=1
      let v=0;
      for (let index = 0; index < this.song4.length; index++){
        if(this.song4[index].id == this.ide){ 
          v=index
        }
      }
      this.fin(this.song4[v])
      if (v==0 ){
        this.sik[0] = this.song4[this.song4.length-1]
        this.sik1=this.sik[0];
        this.ide= this.song4[this.song4.length-1].id
        this.play(this.song4[this.song4.length-1])
      }else{
        this.sik[0]=this.song4[v-1]
        this.sik1=this.sik[0];
        this.ide= this.song4[v-1].id
        this.images = this.song4[v-1].pathA
        this.play(this.song4[v-1])
        this.getDuration(this.song4[v-1])
        variable.song = this.song[v-1]
      }

    }

  }
  async getDuration(objet:any){
    return await NativeAudio.getDuration({
      assetId: objet.id.toString()
    })
  .then(result => {
    this.temprestant_non_convertis = result.duration
  })
  }
  async pausesong(){
    this.isPlay=0
    await NativeAudio.pause({
      assetId: this.sik[0].id.toString(),
    })
  }
  Repeter() {
    if(this.repIcon == 'repeat-outline'){
      this.repIcon = 'repeat-sharp'
      this.repeat();
    }else{
      this.repIcon = 'repeat-outline'
      this.repeat();
    }
  }
  async encour(objet:any){
    let self = this;
    let diff:any=1

    await NativeAudio.getCurrentTime({
      assetId: objet.id.toString()
  })
  .then(resul => {
    this.position = resul.currentTime
    if(this.position == self.temprestant_non_convertis ){
      this.suivant();
      self.fin(objet); 
    }

    if(this.position >= 0 && resul < self.duration){
      if(Math.abs(this.last_position - this.position) >= diff){
        self.curr_paying_file.seekTo(this.last_position * 1000);
      }else{
        self.position = resul.currentTime;
        this.convertcurentime(resul.currentTime)
      }
    }else if (resul >= self.duration){
      self.fin(objet);

      self.play(objet);     
    }
  })
  }
}
