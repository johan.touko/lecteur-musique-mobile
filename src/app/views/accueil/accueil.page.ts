import { Component, OnInit } from '@angular/core';
import { MusiqueService } from 'src/app/services/musique.service';
import { Router } from '@angular/router';
import {NativeAudio} from '@capgo/native-audio';

export let accueilvariable = {
  song:{}
}
@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {
  testposition: number = 0 ;
  favicon = 'heart-outline';
  searche : string = "";
  temprestant!: string;



  public listsong: any;
  public listabum: any;

  constructor(private accueil : MusiqueService , private router : Router) { }

  ngOnInit() {
    this.accueil.getsong().subscribe((rep) =>{
      this.listsong = rep

      this.accueil.getalbums().subscribe((rep) =>{
        this.listabum = rep;
        
        this.listsong.map((e: any) => {
          this.listabum.map((x: any) => {
              if (e.albumid == x.id) {
                e.pathA = x.image
              }
          })
        })
        
      })

      this.listsong.forEach(async(el:any) =>{
              await this.charger(el);
            })
      })  


  }

  async charger(sg:any){
    await NativeAudio.preload({ 
      assetId: sg.id.toString(),
      assetPath: sg.url,
      audioChannelNum: 1, 
      isUrl: false
      });
  }

  fav() {
    if(this.favicon == 'heart-outline'){
      this.favicon = 'heart-sharp'
    }else{
      this.favicon = 'heart-outline'
    }
  }

  async play(sg:any){  
    console.log(accueilvariable.song,"play acueil1")
    this.fin(accueilvariable.song)
    accueilvariable.song = sg
    console.log(accueilvariable.song,"play acueil")

    await NativeAudio.play({ 
      assetId: sg.id.toString(), 
      time: 0.0 
      });  
  }
  async fin(Object:any){
    await NativeAudio.stop({
      assetId: Object.id.toString()
    })
  }
  convertseconde(secondes: number){
    const min = Math.floor((secondes/60/60)*60);
    const sec = Math.floor(((secondes/60/60)*60 - min)*60);
    this.temprestant = min + 'm' + sec + 's'; 
    console.log("yo2" , this.temprestant);

  }

  async getDuration(objet:any){
    return await NativeAudio.getDuration({
      assetId: objet.id.toString()
    })
  .then(result => {
    this.convertseconde(result.duration);
  })
 }

}

