import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccueilPageRoutingModule } from './accueil-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { AccueilPage } from './accueil.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccueilPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [AccueilPage]
})
export class AccueilPageModule {}
