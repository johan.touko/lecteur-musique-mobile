import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListePageRoutingModule } from './liste-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { ListePage } from './liste.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListePageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [ListePage]
})
export class ListePageModule {}
