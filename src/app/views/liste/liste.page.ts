import { Component, OnInit } from '@angular/core';
import { MusiqueService } from 'src/app/services/musique.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-liste',
  templateUrl: './liste.page.html',
  styleUrls: ['./liste.page.scss'],
})
export class ListePage implements OnInit {
  favicon = 'heart-outline';
  searche : string = "";
  public listsong: any;
  public listabum: any;

  constructor(private liste : MusiqueService , private router : Router) { }

  ngOnInit() {
    this.liste.getsong().subscribe((rep) =>{
      this.liste.getalbums().subscribe((rep) =>{
        this.listabum = rep;})
      this.listsong = rep
      })  
      
      
    this.liste.getalbums().subscribe((rep) =>{
        this.listabum = rep;
        console.log(this.listabum)
    
    })
  }

  fav() {
    if(this.favicon == 'heart-outline'){
      this.favicon = 'heart-sharp'
    }else{
      this.favicon = 'heart-outline'
    }
  }
}
