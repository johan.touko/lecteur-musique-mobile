import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children : [
      {
        path: '',
        redirectTo: 'accueil', pathMatch: 'full'
      },
      
      {
        path: 'albums',
        loadChildren: () => import('../../views/albums/albums.module').then( m => m.AlbumsPageModule)
      },
      {
        path: 'accueil',
        loadChildren: () => import('../../views/accueil/accueil.module').then( m => m.AccueilPageModule)
      },
      {
        path: 'a-propos',
        loadChildren: () => import('../../views/a-propos/a-propos.module').then( m => m.AProposPageModule)
      },
      {
        path: 'liste',
        loadChildren: () => import('../../views/liste/liste.module').then( m => m.ListePageModule)
      },
    ]
  },
  {
    path : '',
    redirectTo: 'tabs', pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
