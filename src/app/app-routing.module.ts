import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'tabs',
    pathMatch: 'full'
  },
  {
    path: 'tabs',
    loadChildren: () => import('./views/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'albums',
    loadChildren: () => import('./views/albums/albums.module').then( m => m.AlbumsPageModule)
  },
  {
    path: 'accueil',
    loadChildren: () => import('./views/accueil/accueil.module').then( m => m.AccueilPageModule)
  },
  {
    path: 'a-propos/:id/:url/:albumid/:img/:testposition',
    loadChildren: () => import('./views/a-propos/a-propos.module').then( m => m.AProposPageModule)
  },
  {
    path: 'song',
    loadChildren: () => import('./views/song/song.module').then( m => m.SongPageModule)
  },
  {
    path: 'liste',
    loadChildren: () => import('./views/liste/liste.module').then( m => m.ListePageModule)
  },
  {
    path: 'detailalbums/:id/:titre/:image/:artiste/:color',
    loadChildren: () => import('./views/detailalbums/detailalbums.module').then( m => m.DetailalbumsPageModule)
  },
  // {
  //   path: 'a-propos2/:id/:url/:albumid/:img',
  //   loadChildren: () => import('./views/a-propos2/a-propos2.module').then( m => m.APropos2PageModule)
  // },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
