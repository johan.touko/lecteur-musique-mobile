import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'musique.com',
  appName: 'Musique',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
